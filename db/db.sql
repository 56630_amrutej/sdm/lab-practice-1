CREATE TABLE emp
(
    empid INTEGER PRIMARY KEY AUTO_INCREMENT,
    name varchar(50),
    salary FLOAT,
    age INTEGER
);


INSERT INTO emp (name, salary, age) VALUES ('a', 1, 1);
INSERT INTO emp (name, salary, age) VALUES ('b', 2, 2);
INSERT INTO emp (name, salary, age) VALUES ('c', 3, 3);
INSERT INTO emp (name, salary, age) VALUES ('d', 4, 4);
INSERT INTO emp (name, salary, age) VALUES ('e', 5, 5);