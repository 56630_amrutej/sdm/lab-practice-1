const mysql= require('mysql2')

const pool= mysql.createPool({

    // host:'localhost',
    host:'demodb',  
    user:'root',
    password: 'root',
    // password: 'manager',
    database:'demodb',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit:0,
})

module.exports= pool