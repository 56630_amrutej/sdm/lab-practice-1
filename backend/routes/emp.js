const express= require('express')
const router= express.Router()

const db= require('../db')
const utils= require('../utils')

router.get('/', (request,response)=>{

    const query= `
    select * from emp
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })

})


router.post('/', (request,response)=>{

    const{name, salary, age}= request.body

    const query= `
    insert into emp (name, salary, age) 
    values
    ('${name}','${salary}','${age}')
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })

})


router.put('/:id', (request,response)=>{

    const{id}= request.params
    const{name, salary, age}= request.body

    const query= `
    update emp set
    name= '${name}',
    salary='${salary}',
    age='${age}'
    Where
    empid= '${id}'
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })

})



router.delete('/:id', (request,response)=>{

    const{id}= request.params

    const query= `
    DELETE FROM emp 
    Where
    empid= '${id}'
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })

})











module.exports=router



